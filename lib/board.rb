class Board
  attr_reader :spaces
  def initialize
    @spaces = {} 
    (:a..:g).each{|file| (1..7).each{|rank| @spaces[(file.to_s + rank.to_s).to_sym] = Space.new(rank, file)}}
    populate @spaces
  end

  def populate spaces
    spaces.values.each{|space| space.populate}
  end

  def spaces
    @spaces.values
  end

  def space key
    @spaces[key]
  end

  class Space
    attr_reader :rank, :file, :haven, :occupant
    def initialize rank, file
      @rank = rank
      @file = file
      @occupant = :none
      @open = true
      @haven = set_havens
    end

    def open?
      @occupant == :none
    end

    def raven?
      @occupant == :raven
    end

    def rook?
      @occupant == :rook
    end

    def haven?
      @haven
    end

    def king?
      @occupant == :king
    end

    def set_havens
      return true if ((rank == 1 or rank == 7) and (file == :a or file == :g))
      return false
    end

    def populate
      @occupant = :raven if @rank == 4 or @file == :d
      @occupant = :rook if @rank == 4 and [:c,:e].include?(@file)
      @occupant = :rook if @file == :d and [3,5].include?(@rank)
      @occupant = :king if @file == :d and @rank == 4
    end
  end
end

