$:.unshift File.join(File.dirname(__FILE__),'..','lib')
require 'minitest/autorun'
require 'board'

describe Board do
  let(:allkeys){
    ranks = (:a..:g).to_a.map{|symb| symb.to_s}
    files = (1..7).to_a.map{|symb| symb.to_s}
    ranks.product(files).map{|cell| (cell[0] + cell[1]).to_sym}
  }

  it "should have 49 spaces" do
    Board.new.spaces.length.must_equal 49
  end

  it "should have 36 open spaces" do
    Board.new.spaces.select{|space| space.open?}.length.must_equal 36
  end

  it "should have 7 pieces set on the 4th rank" do
    Board.new.spaces.select{|space| space.rank == 4}.select{|space| !space.open?}.length.must_equal 7
  end

  it "should have 7 pieces set on the :d file" do 
    Board.new.spaces.select{|space| space.file == :d}.select{|space| !space.open?}.length.must_equal 7
  end

  it "should have a haven at each corner" do
    board = Board.new
    [:a1, :a7, :g1, :g7].collect{|key| board.space(key)}.select{|space| space.haven?}.length.must_equal 4
    allkeys.select{|key| board.space(key).haven?}.length.must_equal 4
  end

  it "should have 8 ravens on the board" do
    Board.new.spaces.select{|space| space.raven?}.length.must_equal 8
  end
  
  it" should have 4 ravens  on the 4th rank and 4 on the :d file" do
    board = Board.new
    ravenkeys = [:d1, :d2, :d6, :d7, :a4, :b4, :f4, :g4]
    eightravens = 8.times.inject([]){|arr| arr << :raven}
    ravenkeys.collect{|key| board.space(key)}.collect{|space| space.occupant}.must_equal eightravens
   end

  it "should have 4 rooks on the board" do
    Board.new.spaces.select{|space| space.rook?}.length.must_equal 4
  end

  it "should have a king in the center" do
    Board.new.space(:d4).king?.must_equal true
    (allkeys - [:d4]).collect{|key| Board.new.space(key).king?}.uniq.must_equal [false]
  end
end

